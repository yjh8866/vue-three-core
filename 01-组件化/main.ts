import { createApp } from 'vue'
import App from './App.vue'
import HmButton from './components/HmButton.vue'

const app = createApp(App)

// 注册全局组件
// app.component('组件名', 组件)
// 组件的命名: hm-button HmButton
app.component('hm-button', HmButton)

app.mount('#app')


